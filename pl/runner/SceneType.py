from enum import Enum


class SceneType(Enum):
    MENU = 1
    GAME = 2
