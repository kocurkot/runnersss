import os
import sys

import pygame

from pl.runner.SceneType import SceneType
from pl.runner.scene.GameSceneComposite import GameSceneComposite
from pl.runner.scene.MenuSceneComposite import MenuSceneComposite
from pl.runner.scene.SceneFacade import SceneFacade
from pl.runner.scene.SceneSharedData import SceneSharedData

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.getcwd()))))

from pygame.locals import *


def main():
    pygame.init()

    fps = 60
    screen_width = 800
    screen_height = 600
    size = (screen_width, screen_height)
    screen = pygame.display.set_mode(size, RESIZABLE | HWSURFACE | DOUBLEBUF)

    clock = pygame.time.Clock()
    run = True
    menu_scene = MenuSceneComposite(screen)
    scene_manager = SceneSharedData.Instance()
    scene_manager.actual_scene_type = SceneType.MENU

    while run:

        if scene_manager.actual_scene_type is SceneType.MENU:
            SceneFacade.perform(menu_scene)
        elif scene_manager.actual_scene_type is SceneType.GAME:
            SceneFacade.perform(game_scene)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if scene_manager.actual_scene_type is SceneType.MENU:
                game_scene = GameSceneComposite(screen)
                menu_scene.menu_control_processor(event.type, pygame.mouse.get_pos())

        clock.tick(fps)

        pygame.display.flip()


if __name__ == "__main__":
    main()
