import os
import random
from pl.runner.animation.pyganim import *
from pl.runner.sprite.CustomSprite import CustomSprite


class EnemySprite(CustomSprite):
    def __init__(self):
        super().__init__(self)
        self.__enemy_size = random.randrange(40, 110)
        self.image = self._load_random_animation_from_directory(
            os.path.join('resources', 'enemies'), (self.__enemy_size, self.__enemy_size), 0.1
        )
        self.rect = pygame.Rect(0, 0, self.__enemy_size - 5, self.__enemy_size - 5)
