import os

from pl.runner.animation.pyganim import *
from pl.runner.sprite.CustomSprite import CustomSprite


class PlayerSpriteFlying(CustomSprite):
    def __init__(self, screen):
        super().__init__(screen)

        self.size = (90, 70)
        animation = self._load_animation_from_directory(
            os.path.join('resources', 'player', 'bee', 'flying'), self.size, 0.1
        )

        self.image = animation
        self.rect = pygame.Rect(0, 0, self.size[0] - 5, self.size[1] - 5)
