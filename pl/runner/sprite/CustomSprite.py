import glob
import os
import random

import pygame

from pl.runner.animation.pyganim import PygAnimation


class CustomSprite(pygame.sprite.Sprite):
    def __init__(self, screen):
        super().__init__()
        self.screen = screen

    def _load_random_animation_from_directory(self, directory, size, duration_of_frame):
        directories = os.listdir(directory)
        random_dir_index = random.randrange(0, len(directories))
        random_dir_name = directories[random_dir_index]
        directory = os.path.join(directory, random_dir_name)
        animation = self._load_animation_from_directory(directory, size, duration_of_frame)
        return animation

    def _load_animation_from_directory(self, directory, size, duration_of_frame):
        images = glob.glob(directory + "\*.png")
        images_tuples = []
        for image_path in images:
            images_tuples.append((image_path, duration_of_frame))
        animation = PygAnimation(images_tuples)
        animation.scale(size)
        animation.play()
        return animation
