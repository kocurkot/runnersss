import pygame

from pl.runner import SceneType
from pl.runner.scene.SceneObjectComposite import SceneObjectComposite
from pl.runner.scene.SceneSharedData import SceneSharedData
from pl.runner.scene.component.BackgroundObject import BackgroundObject
from pl.runner.scene.component.EnemyObjectsPrototype import EnemyObjectsPrototype
from pl.runner.scene.component.Hud import Hud
from pl.runner.scene.component.PlayerObject import PlayerObject


class GameSceneComposite(SceneObjectComposite):
    pygame.display.set_caption("Runnersss")

    def __init__(self, screen):
        super().__init__(screen)
        self.background_object = BackgroundObject(self)
        self.add_scene_object(self.background_object)
        self.player_object = PlayerObject(self)
        self.add_scene_object(self.player_object)
        self.enemy_objects_prototype = EnemyObjectsPrototype(self)
        self.add_scene_object(self.enemy_objects_prototype)
        self.hud = Hud(self, pygame.Rect(0, 0, 150, 30), (3, 6))
        self.add_scene_object(self.hud)

    def game_over(self, game_over_listener):
        game_over_listener()
        SceneSharedData.Instance().last_score = self.player_object.distance
        del self
        SceneSharedData.Instance().actual_scene_type = SceneType.MENU
