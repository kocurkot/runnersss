import pygame

from pl.runner import SceneType
from pl.runner.scene.SceneObjectComposite import SceneObjectComposite
from pl.runner.scene.SceneSharedData import SceneSharedData
from pl.runner.scene.component.BackgroundObject import BackgroundObject
from pl.runner.scene.component.MenuLastScoreHud import MenuLastScoreHud
from pl.runner.scene.component.MenuStartButton import MenuStartButton


class MenuSceneComposite(SceneObjectComposite):
    def __init__(self, screen):
        super().__init__(screen)
        self.parallax_background = None
        self.background_object = BackgroundObject(self)
        self.add_scene_object(self.background_object)

        screen_size = screen.get_size()
        self.__width = screen_size[0]
        self.__height = screen_size[1]
        self.__scene_manager = SceneSharedData.Instance()
        self.__hud = MenuLastScoreHud(self)
        self.__start_button = MenuStartButton(self)
        self.add_scene_object(self.__hud)
        self.add_scene_object(self.__start_button)

    def menu_control_processor(self, event_type, mouse_position):
        keys = pygame.key.get_pressed()

        if keys[pygame.K_SPACE] or keys[pygame.K_RETURN]:
            self.__start_game_scene()

        if event_type == pygame.MOUSEBUTTONDOWN:
            if self.__start_button.button.pressed(mouse_position):
                self.__start_game_scene()

    def __start_game_scene(self):
        self.__scene_manager.actual_scene_type = SceneType.GAME
