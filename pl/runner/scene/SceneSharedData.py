from pl.runner.util.Singleton import Singleton


@Singleton
class SceneSharedData:
    def __init__(self):
        self.actual_scene_type = None
        self.last_score = 0
