class SceneObjectComposite(object):
    def __init__(self, screen):
        self.screen = screen
        self._is_started = False
        self._scene_objects = []

    def is_started(self):
        return self._is_started

    def add_scene_object(self, scene_object):
        self._scene_objects.append(scene_object)

    def on_update(self):
        for o in self._scene_objects:
            o.on_update()

    def on_render(self):
        for o in self._scene_objects:
            o.on_render()

    def on_late_update(self):
        for o in self._scene_objects:
            o.on_late_update()

    def on_late_render(self):
        for o in self._scene_objects:
            o.on_late_render()

    def on_start(self):
        for o in self._scene_objects:
            o.on_start()
        self._is_started = True
