import random

from pl.runner.scene.component.GameObject import GameObject
from pl.runner.sprite.EnemySprite import EnemySprite


class EnemyObject(GameObject):
    def __init__(self, screen):
        super().__init__(screen)

    def on_start(self):
        self._sprite = EnemySprite()
        self._scene.enemy_objects_prototype.enemy_sprite_list.add(self._sprite)
        self.__init_position()

    def on_late_update(self):
        pass

    def on_render(self):
        self._sprite.image.blit(self._scene.screen, self._sprite.rect)

    def on_update(self):
        self.movement()
        self.remove_logic()

        collide = self._scene.player_object.check_collisions_with(self._sprite)
        if collide:
            self.__collision_with_player_logic()

    def on_late_render(self):
        pass

    def __init_position(self):
        self._sprite.rect.y = random.randint(0, self._scene.screen.get_size()[1])
        self._sprite.rect.x = self._scene.screen.get_size()[0] - 10

    def movement(self):
        speed = 15
        self._sprite.rect.x -= 0.5 * speed

    def remove_logic(self):
        if self._sprite.rect.x < -20:
            self.__remove_self()

    def __collision_with_player_logic(self):
        self.__remove_self()
        self._scene.player_object.collision_with_enemy = True

    def __remove_self(self):
        self._sprite.image.visibility = False
        self._scene.enemy_objects_prototype.enemy_sprite_list.remove(self._sprite)
        self._scene.enemy_objects_prototype.enemy_objects_list.remove(self)
        del self
