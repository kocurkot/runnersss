from pl.runner.gui.Button import Button
from pl.runner.scene.component.GameObject import GameObject


class MenuStartButton(GameObject):

    def __init__(self, scene):
        super().__init__(scene)
        screen_size = self._scene.screen.get_size()
        self.__width = screen_size[0]
        self.__height = screen_size[1]

    def on_late_render(self):
        pass

    def on_start(self):
        self.button = Button()

    def on_late_update(self):
        pass

    def on_update(self):
        pass

    def on_render(self):
        self.button.create_button(self._scene.screen, (107, 142, 35), self.__width / 2 - 100, self.__height / 2 - 50,
                                          200, 100, 0, "Start", (255, 255, 255))
