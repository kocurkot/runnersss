from pl.runner.parallax import background
from pl.runner.scene.component.GameObject import GameObject


class BackgroundObject(GameObject):

    def __init__(self, scene):
        super().__init__(scene)
        self.parallax_background = None
        self.background_speed = 2
        self.__background_orientation = 'horizontal'

    def on_start(self):
        self.parallax_background = self.__generate_background()

    def on_late_update(self):
        pass

    def on_render(self):
        self.__draw()

    def on_update(self):
        pass

    def on_late_render(self):
        pass

    def __generate_background(self):
        parallax_background = background.create(self._scene.screen.get_size())
        return parallax_background

    def __draw(self):
        self.parallax_background.scroll(self.background_speed, self.__background_orientation)
        self.parallax_background.draw(self._scene.screen)
