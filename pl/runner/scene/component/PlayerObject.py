import pygame

from pl.runner.scene.component.GameObject import GameObject
from pl.runner.sprite.PlayerSpriteFlying import PlayerSpriteFlying


class PlayerObject(GameObject):
    def on_late_update(self):
        pass

    def game_over(self):
        print(self._sprite.rect)

    def on_update(self):
        self.__control_mechanism_processor()
        self.distance += int(round(self.player_speed/10))
        self._scene.hud.data = self.distance
        if self.__is_player_outside_down() or self.collision_with_enemy:
            self._scene.game_over(lambda: self.game_over())

    def on_render(self):
        self._sprite.image.blit(self._scene.screen, self._sprite.rect)

    def on_late_render(self):
        pass

    def on_start(self):
        self._sprite = PlayerSpriteFlying(self._scene.screen)
        self.__generate_player()

    def __init__(self, scene):
        super().__init__(scene)
        self.__max_width = self._scene.screen.get_size()[0]
        self.__max_height = self._scene.screen.get_size()[1]
        self.collision_with_enemy = False
        self.distance = 0

    def __generate_player(self):
        screen_size = self._scene.screen.get_size()
        init_position = (screen_size[0] / 4, screen_size[1] / 2)
        self.__set_player_on_position(init_position)
        self.player_speed = 7

    def __set_player_on_position(self, position):
        self._sprite.rect.x = position[0]
        self._sprite.rect.y = position[1]

    def __control_mechanism_processor(self):
        keys = pygame.key.get_pressed()

        if keys[pygame.K_RIGHT] and not self.__is_player_outside_right():
            self._sprite.rect.x += 1 * self.player_speed * 0.4
        if keys[pygame.K_LEFT] and not self.__is_player_outside_left():
            self._sprite.rect.x -= 1 * self.player_speed * 0.4
        if keys[pygame.K_UP] and not self.__is_player_outside_up():
            self._sprite.rect.y -= 1 * self.player_speed

        self._sprite.rect.y += 0.5 * self.player_speed

    def check_collisions_with(self, checking_object):
        collide = pygame.sprite.collide_circle(self._sprite, checking_object)
        return collide

    def __is_player_outside_right(self):
        player_sprite_width = self._sprite.size[0]
        return self._sprite.rect.x > self.__max_width - player_sprite_width

    def __is_player_outside_left(self):
        return self._sprite.rect.x < 0

    def __is_player_outside_up(self):
        return self._sprite.rect.y < 0

    def __is_player_outside_down(self):
        player_sprite_height = self._sprite.size[1]
        return self._sprite.rect.y > self.__max_height - player_sprite_height
