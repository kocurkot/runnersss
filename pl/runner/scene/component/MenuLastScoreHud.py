import pygame

from pl.runner.scene.SceneSharedData import SceneSharedData
from pl.runner.scene.component.GameObject import GameObject
from pl.runner.scene.component.Hud import Hud


class MenuLastScoreHud(GameObject):

    def __init__(self, scene):
        super().__init__(scene)

    def on_late_render(self):
        self.hud.on_late_render()

    def on_start(self):
        self.hud = Hud(self._scene, pygame.Rect(self._width / 2 - 98, self._height / 2 + 55, 198, 30),
                       (self._width / 2 - 90, self._height / 2 + 61))
        self.hud.on_start()
        self.hud.set_custom_score_txt_prefix("last score: ")

    def on_late_update(self):
        self.hud.on_late_update()

    def on_update(self):
        self.hud.on_update()
        self.hud.data = SceneSharedData.Instance().last_score

    def on_render(self):
        self.hud.on_render()

