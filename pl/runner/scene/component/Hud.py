import pygame
import pygame.gfxdraw

from pl.runner.scene.component.GameObject import GameObject


class Hud(GameObject):
    def __init__(self, scene, rect, x_y):
        super().__init__(scene)
        self.__score_txt_prefix = "score: "
        screen_size = self._scene.screen.get_size()
        self.__rect = rect
        self.__x_y = x_y
        self.__width = screen_size[0]
        self.__height = screen_size[1]
        self.scoreFont = pygame.font.SysFont("Droid Sans", 26)
        self.data = 0

    def on_late_render(self):
        pass

    def on_late_update(self):
        pass

    def on_start(self):
        pass

    def set_custom_score_txt_prefix(self, txt):
        self.__score_txt_prefix = txt

    def on_render(self):
        # distance = self._scene.player_object.distance
        pygame.gfxdraw.box(self._scene.screen, self.__rect, (50, 50, 50, 127))
        distance = self.__score_txt_prefix + str(self.data)
        score_txt_font = self.scoreFont.render(distance, 1, (255, 255, 255))
        self._scene.screen.blit(score_txt_font, self.__x_y)

    def on_update(self):
        pass
