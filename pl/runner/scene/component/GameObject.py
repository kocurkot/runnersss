from abc import ABCMeta, abstractmethod


class GameObject(metaclass=ABCMeta):
    def __init__(self, scene):
        self._scene = scene
        self._sprite = None
        screen_size = self._scene.screen.get_size()
        self._width = screen_size[0]
        self._height = screen_size[1]

    @abstractmethod
    def on_update(self):
        pass

    @abstractmethod
    def on_render(self):
        pass

    @abstractmethod
    def on_late_update(self):
        pass

    @abstractmethod
    def on_late_render(self):
        pass

    @abstractmethod
    def on_start(self):
        pass
