import pygame

from pl.runner.scene.component.EnemyObject import EnemyObject
from pl.runner.scene.component.GameObject import GameObject


class EnemyObjectsPrototype(GameObject):

    def __init__(self, scene):
        super().__init__(scene)
        self.distance = 0
        self.enemy_sprite_list = pygame.sprite.Group()
        self.enemy_objects_list = []

    def on_start(self):
        pass

    def on_late_update(self):
        pass

    def on_render(self):
        for enemy_object in self.enemy_objects_list:
            enemy_object.on_render()

    def on_update(self):
        self.__calculate_destination()
        if self.distance > 700:
            new_enemy = EnemyObject(self._scene)
            new_enemy.on_start()
            self.enemy_objects_list.append(new_enemy)
            self.__reset_current_destination()

        self.__calculate_destination()

        for enemy in self.enemy_objects_list:
            enemy.on_update()

    def on_late_render(self):
        pass

    def __calculate_destination(self):
        self.distance += self._scene.player_object.player_speed

    def __reset_current_destination(self):
        self.distance = 0


