class SceneFacade:
    # Duck Typing
    @staticmethod
    def perform(scene):
        if not scene.is_started():
            scene.on_start()
        scene.on_render()
        scene.on_late_render()
        scene.on_update()
        scene.on_late_update()
