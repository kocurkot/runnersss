import sys
import pygame
import os

from pl.runner.parallax import parallax

sys.path.append("../")


def __path_builder(image_name):
    return os.path.join('resources', 'background', image_name)


def create(resolution):
    background = parallax.ParallaxSurface(resolution, pygame.RLEACCEL)

    tiles_path = __path_builder('Tiles.png')
    clouds_path = __path_builder('Clouds.png')
    far_woods_path = __path_builder('Far_woods.png')
    mountain_path = __path_builder('Mountain.png')
    sky_path = __path_builder('Sky.png')

    background.add(sky_path, 5, resolution)
    background.add(clouds_path, 4, resolution)
    background.add(mountain_path, 3, resolution)
    background.add(far_woods_path, 2, resolution)
    background.add(tiles_path, 1, resolution)
    # background.add(__path_builder('p3.png'), 2)
    # background.add(__path_builder('p1.png'), 1)
    return background

#
# class Background(object):
#     pass
